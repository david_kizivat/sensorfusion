package eu.kizivat.sensorfusionlibrary;

import android.hardware.Sensor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a Step Counter sensor.
 *
 * @author     Dávid Kizivat <david.kizivat@gmail.com>
 * @version    0.2
 * @since      2014-12-1
 */
public abstract class StepCounter extends FusionSensor {

    private static final String TAG = StepCounter.class.getSimpleName();

    private List<StepListener> mListeners = null;
    private int mStepCountAcc = 0;
    private int mStepCountSensor = 0;
    //private boolean mIsFirst = true;
    //private int mFirstValue = 0;

    /**
     * Creates an instance of the StepCounter class.
     */
    public StepCounter() {
        this.mListeners = new ArrayList<>();
    }

    @Override
    public void notifyUpdate(float[] values, long timestamp, int sensorType) {
        // Notify the listener after the specified time since the last event.
        //if (mIsFirst) mFirstValue = (int) values[0];
        Log.d(TAG, Float.toString(values[0]));
        if (sensorType == Sensor.TYPE_ACCELEROMETER)
            mStepCountAcc++;
        if (sensorType == Sensor.TYPE_STEP_DETECTOR)
            mStepCountSensor++;
        if (timestamp-this.getLastEventTimestamp() >= this.getNotifyAfterTime()*1000000 ||
                this.getNotifyAfterTime() == 0) {

            this.setLastEventTimestamp(timestamp);


            Log.d(TAG, mListeners.toString());
            // Notify each of the registered listeners.
            for (StepListener listener : mListeners) {
                if(sensorType == Sensor.TYPE_STEP_DETECTOR)
                    listener.onStepDetected(mStepCountSensor, 1 /* Length will be get by MotionSensor */, timestamp);
                if(sensorType == Sensor.TYPE_ACCELEROMETER)
                    listener.onStepDetected(mStepCountAcc, 2 /* Length will be get by MotionSensor */, timestamp);
            }
            //mIsFirst = false;
        }
    }

    /**
     * Registers a listener for the Compass.
     *
     * @param listener The listener to be registered.
     */
    public void registerListener(StepListener listener) {
        mStepCountSensor = 0;
        mStepCountAcc = 0;
        if (listener == null)  {
            throw new NullPointerException("Listener to register cannot be null.");
        }
        if (!mListeners.add(listener)) {
            Log.d(TAG, listener.toString());
            throw new  IllegalArgumentException("The listener has already been registered.");
        }
        Log.i(TAG, "A new listener has been registered.");
    }

    /**
     * Unregisters a listener of the StepCounter.
     *
     * @param listener The listener to be unregistered.
     */
    public void unregisterListener(StepListener listener) {
        if (listener == null) {
            throw  new NullPointerException("Listener to unregister cannot be null.");
        }
        if (!mListeners.remove(listener)) {
            Log.d(TAG, listener.toString());
            throw new IllegalArgumentException("There is no such a listener registered.");
        }
        Log.i(TAG, "A listener has been unregistered.");
    }

    /**
     * Used for receiving notifications from when StepCounter sensor's values has changed.
     *
     * @author    Dávid Kizivat <david.kizivat@gmail.com>
     * @version   0.2
     * @since     2014-12-1
     */
    public interface StepListener {

        /**
         * Called when a step is given.
         *
         * @param number The number of the give step.
         * @param length Estimated length of the step.
         * @param timestamp Timestamp of the event.
         */
        void onStepDetected(int number, double length, long timestamp);

    }
}
