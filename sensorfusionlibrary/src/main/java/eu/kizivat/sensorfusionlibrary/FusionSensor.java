package eu.kizivat.sensorfusionlibrary;

import android.hardware.Sensor;

import java.util.ArrayList;
import java.util.List;

/**
 * A superclass representing a sensor from the Sensor Fusion Library.
 *
 * @author    Dávid Kizivat <david.kizivat@gmail.com>
 * @version   0.6
 * @since     2014-12-1
 */
public abstract class FusionSensor {

    /**
     * A constant describing a {@link Compass} Fusion Sensor type.
     */
    public static final int TYPE_COMPASS = 0;

    /**
     * A constant describing a {@link StepCounter} Fusion Sensor type.
     */
    public static final int TYPE_STEP_COUNTER = 1;

    /**
     * A constant describing a {@link StepCounterSensor} Fusion Sensor type.
     */
    public static final int TYPE_STEP_COUNTER_SENSOR = 2;

    /**
     * A constant describing a {@link StepCounterAcc} Fusion Sensor type.
     */
    public static final int TYPE_STEP_COUNTER_ACC = 3;

    /**
     * A constant describing a {@link MotionSensor} Fusion Sensor type.
     */
    public static final int TYPE_MOTION_SENSOR = 4;

    /**
     * List of native sensors the FusionSensor uses.
     */
    public List<Sensor> nativeSensorsInUse = null;

    /* Specifies the minimum time to wait until a listener is notified about an sensor update in
       milliseconds. */
    private long mNotifyAfterTime = 0;

    /* The timestamp of the last event that the listener was notified about in nanoseconds. */
    private long mLastEventTimestamp = 0;

    public FusionSensor() {
        nativeSensorsInUse = new ArrayList<>();
    }

    /**
     * Gets the value of time that is a sensor supposed to wait until its listeners are notified about an
     * sensor update (milliseconds).
     *
     * @return The time to wait before update notification in milliseconds.
     */
    public long getNotifyAfterTime() {
        return this.mNotifyAfterTime;
    }

    /**
     * Sets the minimum time the sensor will wait until its listeners are notified about an sensor update
     * (milliseconds).
     *
     * @param millis The time to wait in milliseconds.
     */
    public void setNotifyAfterTime(long millis) {
        this.mNotifyAfterTime = millis;
    }

    /**
     * Gets the timestamp of the latest sensor event that has been sent to the sensor's listeners.
     *
     * @return The timestamp of the latest event.
     */
    public long getLastEventTimestamp() {
        return mLastEventTimestamp;
    }

    /* Protected setter for the mLastEventTimestamp. */
    protected void setLastEventTimestamp(long timestamp) {
        this.mLastEventTimestamp = timestamp;
    }

    /**
     * Notifies a FusionSensor about changes in values. Can be used to simulate a sensor
     * event.
     *
     * @param values The filtered data to be processed.
     * @param timestamp The timestamp of the event.
     */
    public abstract void notifyUpdate(float[] values, long timestamp, int sensorType);
}
