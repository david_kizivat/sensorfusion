package eu.kizivat.sensorfusionlibrary;

/**
 * Heading is a class that carries information about where the device is heading relatively to the
 * both magnetic and geographic north as well as it determines the devices inclination. It represents
 * an event of the {@link Compass} sensor.
 *
 * @author    Dávid Kizivat <david.kizivat@gmail.com>
 * @version   0.4
 * @since     2014-11-22
 */
public class Heading {

    private double mNorthPosition;
    private double mInclination;
    private double mMagneticDeclination;
    private long mTimestamp;

    /**
     * Timestamp of the event.
     *
     * @return The timestamp of the event.
     */
    public long getTimestamp() {
        return mTimestamp;
    }

    /**
     * Heading constructor.
     *
     *  @param northPosition Angular difference between devices Heading (Y axes of the device) and the direction to the Geographic North in degrees.
     * @param mInclination Inclination of the phone.
     * @param magneticDeclination Magnetic Declination of the device's location.
     * @param timestamp Timestamp of the event.
     */
    public Heading(double northPosition, double mInclination, double magneticDeclination, long timestamp) {
        this.mNorthPosition = northPosition;
        this.mInclination = mInclination;
        this.mMagneticDeclination = magneticDeclination;
        this.mTimestamp = timestamp;
    }

    /**
     * Heading constructor.
     *
     *  @param northPosition Angular difference between devices Heading (Y axes of the device) and the direction to the Geographic North in degrees.
     * @param mInclination Inclination of the phone.
     * @param timestamp Timestamp of the event.
     */
    public Heading(double northPosition, double mInclination, long timestamp) {
        this.mNorthPosition = northPosition;
        this.mInclination = mInclination;
        this.mTimestamp = timestamp;
    }

    /**
     *
     * @return North Position in radians.
     */
    public double getNorthPositionRad() {
        return mNorthPosition;
    }

    /**
     *
     * @return North Position in degrees
     */
    public double getNorthPositionDeg() {
        return mNorthPosition*(180/Math.PI);
    }

    /**
     *
     * @return Magnetic Declination in radians.
     */
    public double getMagneticDeclinationRad() {
        return mMagneticDeclination;
    }

    /**
     * @return Magnetic Declination in degrees.
     */
    public double getMagneticDeclinationDeg() {
        return mMagneticDeclination*(180/Math.PI);
    }

    /**
     * @return Inclination in radians.
     */
    public double getInclinationRad() {
        return mInclination;
    }

    /**
     * @return Inclination in degrees.
     */
    public double getInclinationDeg() {
        return mInclination*(180/Math.PI);
    }
}