package eu.kizivat.sensorfusionlibrary;

import jkalman.JKalman;
import jama.Matrix;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.OperationCanceledException;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * FusionSensorManager lets you access {@link FusionSensor}s currently including {@link Compass},
 * {@link StepCounter} and {@link MotionSensor}.
 *
 * @author  Dávid Kizivat <david.kizivat@gmail.com>
 * @version 0.7
 * @since   2014-11-22
 */

public class FusionSensorManager implements SensorEventListener {

    protected static final String TAG = FusionSensorManager.class.getSimpleName();
    private static final String NOT_SUPPORTED = "sensor is not supported by the device.";

    /* Currently active FusionSensors. */
    private List<FusionSensor> mActiveSensors = null;

    /* Android's Sensor Manager */
    private SensorManager mSensorManager = null;

    /* Listening activity's Context */
    private Context mContext = null;

    private int rotationVectorInUse = 0;
    private int stepCounterInUse = 0;
    private int accelerometerInUse = 0;
    private int linAccelerometerInUse = 0;
    private int gravityInUse = 0;

    /* Gravity vector using TYPE_GRAVITY vector */
    private float[] mGravity = null;

    /* Lastly low-pass filtered values */
    private float[] mLowPassedValues = null;

    /* Lastly high-pass filtered values */
    private float[] mHighPassedValues = null;

    /* Lastly measured acceleration values */
    private float[] mLastAccelerationValues = null;

    /* Filter's time-constant [millis] */
    private static final float T = 200.0f;

    /* Filter's constant; alpha = t / (t + dt), where dt is sample rate */
    private float mAlpha = 0.0f;

    /* Structure to keep the Kalman filter's state */
    private  JKalman mKalman = null;

    /**
     * Creates an instance of a FusionSensorManager.
     */
    public FusionSensorManager(Context context) {
        if (context == null) throw new NullPointerException("Context cannot be null");
        mActiveSensors = new ArrayList<>();

        mContext = context;
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
    }

    /**
     * Gets instance of {@link FusionSensor} of given type.
     *
     * @param sensor Type of sensor to get. E.g. TYPE_COMPASS
     * @return Instance of the sensor.
     */
    public FusionSensor getFusionSensor(int sensor) {
        switch (sensor) {
            case FusionSensor.TYPE_COMPASS :
                Log.i(TAG, "Getting Compass.");
                return new Compass();
            case FusionSensor.TYPE_STEP_COUNTER :
                Log.i(TAG, "Getting Step Counter.");
                if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null
                        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
                    return new StepCounterSensor();
                return new StepCounterAcc();
            case FusionSensor.TYPE_STEP_COUNTER_SENSOR :
                Log.i(TAG, "Getting Step Counter (Sensor).");
                if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null
                        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
                    return new StepCounterSensor();
                return new StepCounterSensor();
                //Log.w(TAG, "Sensor TYPE_STEP_COUNTER is not supported by the device. Null returned");
                //return null;
            case FusionSensor.TYPE_STEP_COUNTER_ACC :
                Log.i(TAG, "Getting Step Counter (Accelerometer).");
                return new StepCounterAcc();
            case FusionSensor.TYPE_MOTION_SENSOR :
                Log.i(TAG, "Getting Motion Sensor.");
                return new MotionSensor();
            default :
                throw new IllegalArgumentException("\'" + sensor + "\' is not a constant value" +
                        "for any of the currently implemented FusionSensors.");
        }
    }

    /**
     * Activates the sensor given.
     *
     * @param sensor The {@link FusionSensor} to be activated.
     */
    public void activateSensor(FusionSensor sensor) {
        if (sensor == null) {
            throw  new NullPointerException("The sensor to be activated cannot be null.");
        }

        if (mActiveSensors.contains(sensor)) return;
        // NOTE: Consider using polymorphism here
        if (sensor instanceof Compass) {
            Compass compass = (Compass) sensor;
            if ((mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null)) {
                if (rotationVectorInUse <= 0) {
                    Log.i(TAG, mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR).toString());
                    mSensorManager.registerListener(
                            //listener
                            this,
                            //native sensor
                            mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                            //delay between events
                            SensorManager.SENSOR_DELAY_FASTEST);
                    rotationVectorInUse++;
                }
                sensor.nativeSensorsInUse.add(
                        mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR));
                compass.registerListener((Compass.HeadingListener) mContext);
                mActiveSensors.add(sensor);
                Log.i(TAG, "Compass activated.");
            } else {
                String sensorName = "Rotation Vector ";
                showNotSupportedToast(sensorName);
                Log.w(TAG, sensorName + FusionSensorManager.NOT_SUPPORTED);
            }
        } else if (sensor instanceof StepCounterSensor) {
            StepCounterSensor stepCounter = (StepCounterSensor) sensor;
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
                if (stepCounterInUse <= 0) {
                    Log.i(TAG, mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR).toString());
                    mSensorManager.registerListener(
                            //listener
                            this,
                            //native sensor
                            mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),
                            //delay between events
                            SensorManager.SENSOR_DELAY_FASTEST);
                    stepCounterInUse++;
                }
                sensor.nativeSensorsInUse.add(
                        mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR));
                stepCounter.registerListener((StepCounter.StepListener) mContext);
                mActiveSensors.add(sensor);
                Log.i(TAG, "StepCounter activated.");
            } else {
                String sensorName = "Step Counter ";
                showNotSupportedToast(sensorName);
                Log.w(TAG, sensorName + FusionSensorManager.NOT_SUPPORTED);
            }
        } else if (sensor instanceof StepCounterAcc) {
                mGravity = new float[]{0, 0, 0};
                mLowPassedValues = new float[]{0, 0, 0};
                mHighPassedValues = new float[]{0, 0, 0};
                mLastAccelerationValues = new float[]{0, 0, 0};
                try {
                    mKalman = new JKalman(3, 3);
                } catch (Exception e) {
                    throw new OperationCanceledException("Couldn't activate sensor due to problem while" +
                            "constructing Kalman filter's structure.");
                }
                StepCounterAcc stepCounter = (StepCounterAcc) sensor;
                if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
                    mSensorManager.registerListener(
                            //listener
                            this,
                            //native sensor
                            mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                            //delay between events
                            SensorManager.SENSOR_DELAY_FASTEST);
                }
                /*if (mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null) {
                    if (linAccelerometerInUse <= 0) {
                        Log.i(TAG, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION).toString());
                        mSensorManager.registerListener(
                                //listener
                                this,
                                //native sensor
                                mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                                //delay between events
                                SensorManager.SENSOR_DELAY_FASTEST);
                        linAccelerometerInUse++;
                    }
                    sensor.nativeSensorsInUse.add(
                            mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
                    stepCounter.registerListener((StepCounter.StepListener) mContext);
                    mActiveSensors.add(sensor);
                    Log.i(TAG, "StepCounter activated using linear acceleration sensor logged above.");
                } else*/ if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                    if (accelerometerInUse <= 0) {
                        Log.i(TAG, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER).toString());
                        mSensorManager.registerListener(
                                //listener
                                this,
                                //native sensor
                                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                //delay between events
                                SensorManager.SENSOR_DELAY_FASTEST);
                        accelerometerInUse++;
                    }
                    // Will need rotation vector for eliminating the gravity component from the accelerometer data
                    /*if (mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
                        if (gravityInUse <= 0) {
                            mSensorManager.registerListener(
                                    //listener
                                    this,
                                    //native sensor
                                    mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                                    //delay between events
                                    SensorManager.SENSOR_DELAY_FASTEST);
                            gravityInUse++;
                        }
                    } else {
                        String sensorName = "Gravity ";
                        showNotSupportedToast(sensorName);
                        Log.w(TAG, sensorName + FusionSensorManager.NOT_SUPPORTED);
                    }*/
                    stepCounter.registerListener((StepCounter.StepListener) mContext);
                    mActiveSensors.add(sensor);
                    Log.i(TAG, "MotionSensor activated using accelerometer and gravity sensor.");
                }
            } else if (sensor instanceof MotionSensor) {
                mGravity = new float[]{0, 0, 0};
                mLowPassedValues = new float[]{0, 0, 0};
                mHighPassedValues = new float[]{0, 0, 0};
                mLastAccelerationValues = new float[]{0, 0, 0};
                try {
                    mKalman = new JKalman(3, 3);
                } catch (Exception e) {
                    throw new OperationCanceledException("Couldn't activate sensor due to problem while" +
                            "constructing Kalman filter's structure.");
                }

                MotionSensor motionSensor = (MotionSensor) sensor;
                if (mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null) {
                    if (linAccelerometerInUse <= 0) {
                        Log.i(TAG, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION).toString());
                        mSensorManager.registerListener(
                                //listener
                                this,
                                //native sensor
                                mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                                //delay between events
                                SensorManager.SENSOR_DELAY_FASTEST);
                        linAccelerometerInUse++;
                    }
                    sensor.nativeSensorsInUse.add(
                            mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
                    motionSensor.registerListener((MotionSensor.MoveListener) mContext);
                    mActiveSensors.add(sensor);
                    Log.i(TAG, "MotionSensor activated using linear acceleration sensor logged above.");
                } else if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                    if (accelerometerInUse <= 0) {
                        Log.i(TAG, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER).toString());
                        mSensorManager.registerListener(
                                //listener
                                this,
                                //native sensor
                                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                //delay between events
                                SensorManager.SENSOR_DELAY_FASTEST);
                        accelerometerInUse++;
                    }
                    // Will need rotation vector for eliminating the gravity component from the accelerometer data
                    if (mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
                        if (gravityInUse <= 0) {
                            mSensorManager.registerListener(
                                    //listener
                                    this,
                                    //native sensor
                                    mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                                    //delay between events
                                    SensorManager.SENSOR_DELAY_FASTEST);
                            gravityInUse++;
                        }
                    } else {
                        String sensorName = "Gravity ";
                        showNotSupportedToast(sensorName);
                        Log.w(TAG, sensorName + FusionSensorManager.NOT_SUPPORTED);
                    }
                    motionSensor.registerListener((MotionSensor.MoveListener) mContext);
                    mActiveSensors.add(sensor);
                    Log.i(TAG, "MotionSensor activated using accelerometer and gravity sensor.");
                } else {
                    String sensorName = "Accelerometer ";
                    showNotSupportedToast(sensorName);
                    Log.w(TAG, sensorName + FusionSensorManager.NOT_SUPPORTED);
                }
            }

        }

    private void showNotSupportedToast(String sensorString) {
        sensorString += FusionSensorManager.NOT_SUPPORTED;
        Toast toast = Toast.makeText(mContext, sensorString, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Deactivates the sensor given.
     *
     * @param sensor The sensor to be deactivated.
     */
    public void deactivateSensor(FusionSensor sensor) {
        // NOTE: Consider using polymorphism here too.
        if (sensor instanceof Compass) {
            mSensorManager.unregisterListener(
                    this,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR));
            ((Compass) sensor).unregisterListener((Compass.HeadingListener) mContext);
            mActiveSensors.remove(sensor);
            rotationVectorInUse--;
            Log.i(TAG, "Compass deactivated.");
        } else if (sensor instanceof StepCounterSensor) {
            mSensorManager.unregisterListener(
                    this,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER));
            ((StepCounter) sensor).unregisterListener((StepCounter.StepListener) mContext);
            mActiveSensors.remove(sensor);
            stepCounterInUse--;
            Log.i(TAG, "StepCounter deactivated.");
        } else if (sensor instanceof MotionSensor) {
            mSensorManager.unregisterListener(
                this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
            accelerometerInUse--;
            mSensorManager.unregisterListener(
                this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));
            gravityInUse--;
            mSensorManager.unregisterListener(
                this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
            gravityInUse--;
            ((MotionSensor) sensor).unregisterListener((MotionSensor.MoveListener) mContext);
            Log.i(TAG, "MotionSensor deactivated.");
        }
    }

    /**
     * Deactivates all sensors.
     */
    public void deactivateAllSensors() {
        mSensorManager.unregisterListener(this);
        for (FusionSensor sensor : mActiveSensors) {
            // NOTE: Maybe polymorphism here again?
            if (sensor instanceof Compass) {
                ((Compass) sensor).unregisterListener((Compass.HeadingListener) mContext);
            } else if (sensor instanceof StepCounter) {
                ((StepCounter) sensor).unregisterListener((StepCounter.StepListener) mContext);
            } else if (sensor instanceof MotionSensor) {
                ((MotionSensor) sensor).unregisterListener((MotionSensor.MoveListener) mContext);
            }
        }
        accelerometerInUse = 0;
        linAccelerometerInUse = 0;
        rotationVectorInUse = 0;
        gravityInUse = 0;
        stepCounterInUse = 0;
        mActiveSensors = null;
    }

    /**
     * Calibrates all sensors.
     */
    public void calibrateSensors()
    {
        // TODO: Method implementation.
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ROTATION_VECTOR:
                float[] rotationVectorValues = event.values.clone();
                for (FusionSensor sensor : mActiveSensors) {
                    if (sensor instanceof Compass || sensor instanceof StepCounterAcc) {
                        sensor.notifyUpdate(rotationVectorValues, event.timestamp, event.sensor.getType());
                    }
                }
                break;
            case Sensor.TYPE_STEP_DETECTOR:
                float[] stepCounterValues = event.values.clone();
                Log.i(TAG, "stepcounter event");
                for (FusionSensor sensor : mActiveSensors) {
                    if (sensor instanceof  StepCounterSensor) {
                        sensor.notifyUpdate(stepCounterValues, event.timestamp, event.sensor.getType());
                    }
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                float[] magentometerValues = event.values.clone();
                for (FusionSensor sensor : mActiveSensors) {
                    if ((sensor instanceof  StepCounterAcc))
                        ((StepCounterAcc) sensor).magneticDetector(magentometerValues, event.timestamp / (500 * 10 ^ 6l));
                }
                break;
            case Sensor.TYPE_ACCELEROMETER:
                float[] accelerometerValues = event.values.clone();
                for (FusionSensor sensor : mActiveSensors) {
                    if ((sensor instanceof MotionSensor) || (sensor instanceof StepCounterAcc)) {
                        mAlpha = T / (T + (float) sensor.getNotifyAfterTime());

                        /* Filtration */
                        if (sensor instanceof MotionSensor)
                            accelerometerValues = eliminateGravity(accelerometerValues);
                        accelerometerValues = kalmanFilterRoutine(accelerometerValues);
                        accelerometerValues = highPass(accelerometerValues);
                        accelerometerValues = lowPass(accelerometerValues);

                        if ((sensor instanceof MotionSensor))
                            sensor.notifyUpdate(accelerometerValues, event.timestamp, event.sensor.getType());
                        if ((sensor instanceof  StepCounterAcc))
                            ((StepCounterAcc) sensor).accelDetector(accelerometerValues, event.timestamp / (500 * 10 ^ 6l));
                    }

                }
                break;
            case Sensor.TYPE_GRAVITY:
                mGravity = event.values.clone();
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                float[] linearAccelerationValues = event.values.clone();
                for (FusionSensor sensor : mActiveSensors) {
                    if ((sensor instanceof MotionSensor) || (sensor instanceof StepCounterAcc)) {
                        mAlpha = T / (T + (float) sensor.getNotifyAfterTime());

                    /* Filtration */
                        linearAccelerationValues = kalmanFilterRoutine(linearAccelerationValues);
                        linearAccelerationValues = highPass(linearAccelerationValues);
                        linearAccelerationValues = lowPass(linearAccelerationValues);

                        if ((sensor instanceof MotionSensor))
                            sensor.notifyUpdate(linearAccelerationValues, event.timestamp, event.sensor.getType());
                        /*if ((sensor instanceof  StepCounterAcc))
                            ((StepCounterAcc) sensor).accelDetector(linearAccelerationValues, event.timestamp / (500 * 10 ^ 6l));*/
                    }
                }
                break;
            default:
                throw new IllegalArgumentException("There is some undesired sensor pushing events here!");
        }
    }

    private float[] eliminateGravity(float[] accelerometerValues) {
        accelerometerValues[0] -= mGravity[0];
        accelerometerValues[1] -= mGravity[1];
        accelerometerValues[2] -= mGravity[2];

        return accelerometerValues;
    }

    private float[] lowPass(float[] values) {
        mLowPassedValues[0] = mLowPassedValues[0] * (1.0f - mAlpha) + values[0] * mAlpha;
        mLowPassedValues[1] = mLowPassedValues[1] * (1.0f - mAlpha) + values[1] * mAlpha;
        mLowPassedValues[2] = mLowPassedValues[2] * (1.0f - mAlpha) + values[2] * mAlpha;

        return mLowPassedValues;
    }

    private float[] highPass(float[] values) {
        mHighPassedValues[0] = mAlpha * (mHighPassedValues[0] + values[0] - mLastAccelerationValues[0]);
        mHighPassedValues[1] = mAlpha * (mHighPassedValues[1] + values[1] - mLastAccelerationValues[1]);
        mHighPassedValues[2] = mAlpha * (mHighPassedValues[2] + values[2] - mLastAccelerationValues[2]);

        mLastAccelerationValues[0] = values[0];
        mLastAccelerationValues[1] = values[1];
        mLastAccelerationValues[2] = values[2];

        return mHighPassedValues;
    }

    private float[] kalmanFilterRoutine(float[] values) {
        mKalman.Predict();
        double[] valuesDouble = new double[] { // To create a measurement matrix for Kalman filter Correct we need double[3] vector
                (double)values[0],
                (double)values[1],
                (double)values[2]};
        Matrix valuesMatrix = new Matrix(valuesDouble, 3);
        double[] kalmanOutput = mKalman.Correct(valuesMatrix).getRowPackedCopy();
        for (int i = 0; i < values.length; i++) {
            values[i] = (float)kalmanOutput[i];
        }

        return values;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
