package eu.kizivat.sensorfusionlibrary;

import android.hardware.SensorManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import eu.kizivat.sensorfusionlibrary.FusionSensor;
import eu.kizivat.sensorfusionlibrary.Heading;

/**
 * Class representing the Compass sensor.
 *
 * @author    Dávid Kizivat <david.kizivat@gmail.com>
 * @version   0.7
 * @since     2014-12-1
 */
public class Compass extends FusionSensor {

    private static final String TAG = Compass.class.getSimpleName();

    private List<HeadingListener> mListeners = null;

    /**
     * Creates an instance of the Compass class.
     */
    public Compass() {
        super();
        mListeners = new ArrayList<>();
    }

    @Override
    public void notifyUpdate(float[] data, long timestamp, int sensorType) {
        float[] rotationMatrix = new float[9];
        float[] orientationValues = new float[3];

        SensorManager.getRotationMatrixFromVector(rotationMatrix, data);
        SensorManager.getOrientation(rotationMatrix, orientationValues);

        // The heading to be sent to the listeners.
        Heading heading = new Heading(orientationValues[0], orientationValues[1], timestamp);

        // Notify the listener after the specified time since the last event.
        if (heading.getTimestamp()-this.getLastEventTimestamp() >= this.getNotifyAfterTime()*1000000
                || this.getNotifyAfterTime() == 0) {

            this.setLastEventTimestamp(heading.getTimestamp());

            // Notify each of the registered listeners.
            for (HeadingListener listener : mListeners) listener.onHeadingChanged(heading);
        } // END if
    }

    /**
     * Registers a listener for the Compass.
     *
     * @param listener The listener to be registered.
     */
    public void registerListener(HeadingListener listener) {
        if (listener == null)  {
            throw new NullPointerException("The listener to be registered cannot be null.");
        }
        if (!mListeners.add(listener)) {
            throw new  IllegalArgumentException("The listener has already been registered.");
        }
        Log.i(TAG, "A new listener has been registered.");
    }

    /**
     * Unregisters a listener of the Compass.
     *
     * @param listener The listener to be unregistered.
     */
    public void unregisterListener(HeadingListener listener) {
        if (listener == null) {
            throw  new NullPointerException("The Listener to be unregistered cannot be null.");
        }
        if (!mListeners.remove(listener)) {
            throw new IllegalArgumentException("There is no such a listener registered.");
        }
        Log.i(TAG, "A listener has been unregistered.");
    }


    /**
     * Used for receiving notifications when {@link Compass} sensor's values has changed.
     *
     * @author     Dávid Kizivat <david.kizivat@gmail.com>
     * @version    0.1
     * @since      2014-12-1
     */
    public interface HeadingListener {

        /**
         * Called when sensor values have changed.
         *
         * @param heading Current Heading.
         */
        public void onHeadingChanged(Heading heading);
    }
}
