package eu.kizivat.sensorfusionlibrary;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the Motion sensor.
 *
 * @author     Dávid Kizivat <david.kizivat@gmail.com>
 * @version    0.1
 * @since      2014-12-1
 */
public class MotionSensor extends FusionSensor {

    private static final String TAG = MotionSensor.class.getSimpleName();

    private List<MoveListener> mListeners = null;

    /**
     * Creates an instance of the MotionSensor class.
     */
    public MotionSensor() {
        this.mListeners = new ArrayList<>();
    }

    @Override
    public void notifyUpdate(float[] values, long timestamp, int sensorType) {
        // Notify the listener after the specified time since the last event.
        if (timestamp-this.getLastEventTimestamp() >= this.getNotifyAfterTime()*1000000 ||
                this.getNotifyAfterTime() == 0) {

            setLastEventTimestamp(timestamp);

            // Notify each of the registered listeners.
            for (MoveListener listener : mListeners) {
                listener.onMoveDetected(values, timestamp);
            }
        }

    }

    /**
     * Registers a listener for the Motion.
     *
     * @param listener The listener to be registered.
     */
    public void registerListener(MoveListener listener) {
        if (listener == null)  {
            throw new NullPointerException("Listener to register cannot be null.");
        }
        if (!mListeners.add(listener)) {
            throw new  IllegalArgumentException("The listener has already been registered.");
        }
        Log.i(TAG, "A new listener has been registered.");
    }

    /**
     * Unregisters a listener of the Motion.
     *
     * @param listener The listener to be unregistered.
     */
    public void unregisterListener(MoveListener listener) {
        if (listener == null) {
            throw  new NullPointerException("Listener to unregister cannot be null.");
        }
        if (!mListeners.remove(listener)) {
            throw new IllegalArgumentException("There is no such a listener registered.");
        }
        Log.i(TAG, "A listener has been unregistered.");
    }

    /**
     * Used for receiving notifications from when {@link MotionSensor}'s values has changed.
     *
     * @author    Dávid Kizivat <david.kizivat@gmail.com>
     * @version   0.1
     * @since     2014-12-1
     */
    public interface MoveListener {

        /**
         * Called whenever a move is detected.
         *
         * @param values    X, Y and Z axes changes.
         * @param timestamp Timestamp of the event.
         */
        void onMoveDetected(float[] values, long timestamp);
    }
}
