package eu.kizivat.sensorfusion;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import eu.kizivat.sensorfusionlibrary.FusionSensor;
import eu.kizivat.sensorfusionlibrary.FusionSensorManager;
import eu.kizivat.sensorfusionlibrary.StepCounter;
import eu.kizivat.sensorfusionlibrary.StepCounterAcc;
import eu.kizivat.sensorfusionlibrary.StepCounterSensor;


public class StepCounterActivity extends ActionBarActivity implements StepCounter.StepListener {

    private TextView vTextView = null;
    private TextView vTextView2 = null;
    private FusionSensorManager mSensorManager = null;
    private StepCounterSensor mStepCounterSensor = null;
    private StepCounterAcc mStepCounterAcc = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_counter);

        this.vTextView = (TextView) findViewById(R.id.textView);
        this.vTextView2 = (TextView) findViewById(R.id.textView2);

        this.mSensorManager = new FusionSensorManager(this);
        this.mStepCounterSensor = (StepCounterSensor) mSensorManager.getFusionSensor(FusionSensor.TYPE_STEP_COUNTER_SENSOR);
        this.mStepCounterAcc = (StepCounterAcc) mSensorManager.getFusionSensor(FusionSensor.TYPE_STEP_COUNTER_ACC);
        mStepCounterSensor.setNotifyAfterTime(0);
        mStepCounterAcc.setNotifyAfterTime(0);

        mSensorManager.activateSensor(mStepCounterSensor);
        mSensorManager.activateSensor(mStepCounterAcc);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //mSensorManager.activateSensor(mStepCounter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //mSensorManager.deactivateSensor(mStepCounter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_step_counter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStepDetected(int number, double length, long timestamp) {

        //Log.i("sca", "step");
        if (length == 1)
            vTextView.setText(String.valueOf(number));

        if (length == 2)
            vTextView2.setText(String.valueOf(number));
    }
}
