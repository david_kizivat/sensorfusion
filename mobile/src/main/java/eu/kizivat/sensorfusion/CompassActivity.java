package eu.kizivat.sensorfusion;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import eu.kizivat.sensorfusionlibrary.Compass;
import eu.kizivat.sensorfusionlibrary.FusionSensor;
import eu.kizivat.sensorfusionlibrary.FusionSensorManager;
import eu.kizivat.sensorfusionlibrary.Heading;

public class CompassActivity extends ActionBarActivity implements Compass.HeadingListener {

    protected static final String TAG = CompassActivity.class.getSimpleName();

    private FusionSensorManager mSensorManager = null;
    private Compass mCompass = null;

    private ImageView vImageView = null;
    private TextView vHeadingTextView = null;
    private TextView vInclinationTextView = null;

    private float mCurrentDegree = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);

        vImageView = (ImageView) findViewById(R.id.compass_image_view);
        vHeadingTextView = (TextView) findViewById(R.id.heading_text_view);
        vInclinationTextView = (TextView) findViewById(R.id.inclination_text_view);

        mSensorManager = new FusionSensorManager(this);
        mCompass = (Compass) mSensorManager.getFusionSensor(FusionSensor.TYPE_COMPASS);
        mCompass.setNotifyAfterTime(33);


        mSensorManager.activateSensor(mCompass);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.activateSensor(mCompass);
        Log.i(TAG, "Compass reactivated.");
    }
    @Override
    protected void onPause() {
        super.onPause();

        mSensorManager.deactivateSensor(mCompass);
        Log.i(TAG, "Compass deactivated.");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_compass, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onHeadingChanged(Heading heading) {
        int north = (int) Math.round(heading.getNorthPositionDeg());
        int inc = (int) Math.round(heading.getInclinationDeg());

        vHeadingTextView.setText(Long.toString(north) + "°");
        vInclinationTextView.setText("Inclination: " + Long.toString(inc) + "°");


        RotateAnimation ra = new RotateAnimation(
                mCurrentDegree,
                -north,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        ra.setDuration(33);

        ra.setFillAfter(true);

        vImageView.startAnimation(ra);
        mCurrentDegree = -north;
    }
}
