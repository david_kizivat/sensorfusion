package eu.kizivat.sensorfusion;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import eu.kizivat.sensorfusionlibrary.FusionSensor;
import eu.kizivat.sensorfusionlibrary.FusionSensorManager;
import eu.kizivat.sensorfusionlibrary.MotionSensor;




public class MotionSensorActivity extends ActionBarActivity implements MotionSensor.MoveListener {

    private static final String TAG = MotionSensorActivity.class.getSimpleName();
    private FusionSensorManager mSensorManager = null;
    private MotionSensor mMotionSensor = null;

    private ImageView vBulletXY = null;
    private ImageView vBulletXZ = null;

    private float[] mLastPosition = new float[] {0.0f, 0.0f, 0.0f};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion_sensor);

        vBulletXY = (ImageView) findViewById(R.id.motion_bullet_image_view);
        vBulletXZ = (ImageView) findViewById(R.id.motion_bullet_z_image_view);

        this.mSensorManager = new FusionSensorManager(this);
        this.mMotionSensor = (MotionSensor) mSensorManager.getFusionSensor(FusionSensor.TYPE_MOTION_SENSOR);

        mMotionSensor.setNotifyAfterTime(50);
        Log.d(TAG, "created");
        mSensorManager.activateSensor(mMotionSensor);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "resumed");
        mSensorManager.activateSensor(mMotionSensor);
    }

    @Override
         protected void onPause() {
        super.onPause();
        mSensorManager.deactivateSensor(mMotionSensor);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_motion_sensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMoveDetected(float[] values, long timestamp) {

        TranslateAnimation taxy = new TranslateAnimation(
                mLastPosition[0], -values[0]*40,
                mLastPosition[1], values[1]*40);

        taxy.setDuration(33);
        taxy.setFillAfter(true);

        TranslateAnimation taxz = new TranslateAnimation(
                mLastPosition[0], -values[0]*40,
                mLastPosition[2], values[2]*40);

        taxz.setDuration(33);
        taxz.setFillAfter(true);

        vBulletXY.startAnimation(taxy);
        vBulletXZ.startAnimation(taxz);

        mLastPosition[0] = -values[0]*40;
        mLastPosition[1] = values[1]*40;
        mLastPosition[2] = values[2]*40;
    }
}
