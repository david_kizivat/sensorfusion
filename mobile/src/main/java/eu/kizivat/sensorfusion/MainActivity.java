package eu.kizivat.sensorfusion;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private ArrayAdapter<String> mSensorsAdapter;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            String[] sensors = new String[] {
                    "Compass",
                    "Step Counter",
                    "Motion Sensor"
            };

            List<String> sensorsList = new ArrayList<>(Arrays.asList(sensors));

            mSensorsAdapter = new ArrayAdapter<>(
                    getActivity(),
                    R.layout.list_item_fusion_sensor,
                    R.id.list_item_fusion_sensor_textview,
                    sensorsList);

            ListView listView = (ListView) rootView.findViewById(R.id.list_fusion_sensors);
            listView.setAdapter(mSensorsAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String sensor = mSensorsAdapter.getItem(position);
                    Intent intent = null;
                    switch (sensor) {
                        case "Compass":
                            intent = new Intent(getActivity(), CompassActivity.class);
                            break;
                        case "Step Counter":
                            intent = new Intent(getActivity(), StepCounterActivity.class);
                            break;
                        case "Motion Sensor":
                            intent = new Intent(getActivity(), MotionSensorActivity.class);
                            break;
                    }

                    startActivity(intent);
                }
            });
            return rootView;
        }
    }
}
