SensorFusion
============
This is the implementation part of my bachelor's thesis at Faculty of Informatics Masaryk University
, Brno, Czech Republic titled 'Sensor Fusion for Android'. It contains a Sensor
Fusion library and a demo app for Android devices.

The library itself is supposed to make your work with the sensors on Android platform at least a bit less
painful and efficient using sensor fusion and data filtering. Its source code is located in the
`sensorfusionlibrary` folder.

It contains tools that provides you with the most requested data and services from the Android sensor
   framework e.g. **compass** (determining the north position), **step counter** (despite being
   implemented in Android API level 19 as sensor TYPE\_STEP\_COUNTER, most devices still don't support
   the sensor) and **motion sensor** (determining the acceleration of the device in any direction).